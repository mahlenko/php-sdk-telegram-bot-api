<?php

use tgbot\TelegramApi\BotClient;
use tgbot\TelegramApi\Telegram\Methods\Edit\EditMessageMedia;
use tgbot\TelegramApi\Telegram\Types\Media\Photo;

include_once '../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$client = new BotClient(getenv('BOT_TOKEN'));

dump($client->run(
    new EditMessageMedia([
        'chat_id' => getenv('CHAT_ID'),
        'message_id' => 612,
        'media' => new Photo(['media' => 'documents/test2.jpg'])
    ])
) );

