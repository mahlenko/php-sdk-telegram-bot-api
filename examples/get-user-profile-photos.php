<?php

use tgbot\TelegramApi\BotClient;
use tgbot\TelegramApi\Telegram\Methods\Get\GetUserProfilePhotos;

include_once '../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$client = new BotClient(getenv('BOT_TOKEN'));

dump(
    $client->run(
        new GetUserProfilePhotos([
            'user_id' => getenv('CHAT_ID')
        ])
    )
);
