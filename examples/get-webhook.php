<?php

use tgbot\TelegramApi\BotClient;

include_once '../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$client = new BotClient(getenv('BOT_TOKEN'));
$update = $client->webhook();

dump($update);
