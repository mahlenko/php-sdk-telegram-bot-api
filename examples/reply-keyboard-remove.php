<?php

use tgbot\TelegramApi\BotClient;
use tgbot\TelegramApi\Telegram\Methods\Send\SendMessage;
use tgbot\TelegramApi\Telegram\Types\Keyboards\ReplyKeyboardRemove;

include_once '../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$client = new BotClient(getenv('BOT_TOKEN'));

dump($client->run(
    new SendMessage([
        'chat_id' => getenv('CHAT_ID'),
        'text' => 'Keyboard remove.',
        'reply_markup' => new ReplyKeyboardRemove()
    ])
));
