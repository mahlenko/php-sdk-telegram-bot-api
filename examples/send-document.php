<?php

use tgbot\TelegramApi\BotClient;
use tgbot\TelegramApi\Telegram\Methods\Send\SendDocument;
use tgbot\TelegramApi\Telegram\Types\InputFile;

include_once '../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$client = new BotClient(getenv('BOT_TOKEN'));

dump($client->run(
    new SendDocument([
        'chat_id' => getenv('CHAT_ID'),
        'document' => new InputFile('documents/test.png'),
        'caption' => 'Document caption'
    ])
));
