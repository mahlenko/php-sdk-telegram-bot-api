<?php

use tgbot\TelegramApi\BotClient;
use tgbot\TelegramApi\Telegram\Methods\Send\SendMessage;

include_once '../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$client = new BotClient(getenv('BOT_TOKEN'));

dump($client->run(
    new SendMessage([
        'chat_id' => getenv('CHAT_ID'),
        'text' => 'Test message with keyboard',
        'reply_markup' => [ // keyboard
            'resize_keyboard' => true,
            'keyboard' => [
                [ ['text' => '👨🏼‍💼 Profile'], ['text' => '🛎 Notification'] ],
                [ ['text' => '🤖 About'] ]
            ]
        ]
    ])
));
