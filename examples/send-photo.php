<?php

use tgbot\TelegramApi\BotClient;
use tgbot\TelegramApi\Telegram\Methods\Send\SendPhoto;

include_once '../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$client = new BotClient(getenv('BOT_TOKEN'));

dump($client->run(
    new SendPhoto([
        'chat_id' => getenv('CHAT_ID'),
        'caption' => 'Image caption',
        'photo' => 'documents/test.png'
    ])
));
