<?php

use tgbot\TelegramApi\BotClient;
use tgbot\TelegramApi\Telegram\Methods\Send\SendPoll;

include_once '../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$client = new BotClient(getenv('BOT_TOKEN'));

dump($client->run(
    new SendPoll([
        'chat_id' => getenv('CHAT_ID'),
        'question' => 'What year is it now?',
        'options' => range(2018, 2023)
    ])
));