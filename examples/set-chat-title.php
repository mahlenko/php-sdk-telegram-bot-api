<?php

use tgbot\TelegramApi\BotClient;
use tgbot\TelegramApi\Telegram\Methods\Set\SetChatTitle;

include_once '../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$client = new BotClient(getenv('BOT_TOKEN'));

dump($client->run(
    new SetChatTitle([
        'chat_id' => getenv('CHAT_ID'),
        'title' => 'New title set'
    ])
));
