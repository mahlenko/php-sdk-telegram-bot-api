<?php

use tgbot\TelegramApi\BotClient;
use tgbot\TelegramApi\Telegram\Methods\Set\SetWebhook;

include_once '../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$client = new BotClient(getenv('BOT_TOKEN'));

dump($client->run(
    new SetWebhook([
        'url' => getenv('BOT_WEBHOOK'),
//        'certificate' => 'fullchain.pem'
    ])
));
