<?php

namespace tgbot\TelegramApi\Abstracts;

/**
 * @package tgbot\TelegramApi
 */
abstract class TelegramTypesAbstract
{
    /**
     * @param $data
     */
    public function __construct($data = [])
    {
        $this->fillObjectData($data);
    }

    /**
     * @param array $data
     */
    protected function fillObjectData($data = [])
    {
        /* object fill rules */
        $rules = $this->getRules();

        if ($data) {
            foreach ($data as $key => $value) {
                /* filling */
                if (key_exists($key, $rules)) {
                    /* this data array */
                    if ($rules[$key]['is_array']) {
                        foreach ($value as $k => $v) {
                            $value[$k] = new $rules[$key]['class']($v);
                        }
                    } else {
                        $value = new $rules[$key]['class']($value);
                    }
                }

                /* default data */
                $this->$key = $value;
            }
        }

        // remove empty data
        $allow_variables = array_filter((array) $this);
        $clear_variables = array_diff_key((array) $this, $allow_variables);
        foreach ($clear_variables as $key => $value) {
            unset($this->$key);
        }
    }

    /**
     * @return array
     */
    private function getRules()
    {
        $rules = [];
        foreach ($this->rules() as $class => $keys)
        {
            if (!is_array($keys)) {
                $keys = [$keys];
            }

            foreach ($keys as $key)
            {
                $var = $key;
                $is_array = false;

                if ($offset = strpos($key, ':')) {
                    $is_array = true;
                    $var = substr($key, 0, $offset);
                }

                $rules[$var] = ['class' => $class, 'is_array' => $is_array];
            }
        }

        return $rules;
    }

    /**
     * @return mixed
     */
    abstract public function rules();
}