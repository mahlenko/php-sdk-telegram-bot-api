<?php


namespace tgbot\TelegramApi;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;
use tgbot\TelegramApi\Telegram\Types\Chat;
use tgbot\TelegramApi\Telegram\Types\Update;
use tgbot\TelegramApi\Telegram\Types\User;

class BotClient
{
    /**
     * @var string
     */
    private $token = '';

    /**
     * @var string
     */
    private $api_url = 'https://api.telegram.org/bot';

    /**
     * array ['token', 'chat_id']
     * @var bool|array
     */
    private $test = false;

    /**
     * @var Client
     */
    public $client;

    /**
     * @var
     */
    public $type;

    /**
     * @var Chat
     */
    public $chat;

    /**
     * @var User
     */
    public $from;

    /**
     * @var Update
     */
    public $update;

    /**
     * @var string[]
     */
    protected $update_types = [
        'message',
        'edited_message',
        'channel_post',
        'edited_channel_post',
        'inline_query',
        'chosen_inline_result',
        'callback_query',
        'shipping_query',
        'pre_checkout_query',
        'poll',
        'poll_answer'
    ];

    /**
     * BotClient constructor.
     * @param string $token
     * @param array $client_options
     */
    public function __construct(string $token, array $client_options = [])
    {
        $this->token = $token;
        $this->client = new Client($client_options);
    }

    /**
     *
     */
    public function webhook()
    {
        /* Get updates */
        $this->update = new Update(
            json_decode(file_get_contents('php://input'))
        );

        /* Search type update */
        foreach ($this->update as $key => $data) {
            if (array_search($key, $this->update_types) !== false)
            {
                $this->type = $key;
                if (isset($data->chat)) {
                    $this->chat = $data->chat;
                }

                if (isset($data->from)) {
                    $this->from = $data->from;
                }
            }
        }

        return $this->update;
    }

    /**
     * Run method request to Telegram API
     * @param TelegramMethodsAbstract $method
     * @return array|mixed|string[]
     */
    public function run(TelegramMethodsAbstract $method)
    {
        /* */
        try {
            $method->requireFields();
        } catch (Exception $e) {
            $this->update = [
                'ok' => false,
                'description' => $e->getMessage()
            ];
        }

        /* */
        $method->beforeSending();

        /* */
        $query_params = $this->getParamsQuery($method);

        /*  */
        $this->test($query_params);

        /* get method name */
        $method_name = get_class($method);
        $method_name = substr($method_name, strrpos($method_name, '\\') + 1);

        /* send telegram api */
        try {
            $response = $this->client->post($this->compareApiMethod() .'/'. $method_name, $query_params);
        } catch (RequestException $e) {
            return json_decode( // error Telegram
                $e->getResponse()->getBody()->getContents()
            );
        }

        if ($response->getStatusCode() !== 200) {
            $this->update = [
                'ok' => 'false',
                'description' => 'Get status code: ' . $response->getStatusCode()
            ];

            return $this->update;
        }

        try {
            $content = $response->getBody()->getContents();
            $content = json_decode($content);
            if ($content->ok) {
                $content->result = $method->bindToObject((array) $content->result);
            }

            return $content;
        } catch (Exception $e) {
            $this->update = [
                'ok' => 'false',
                'description' => $e->getMessage()
            ];

            return $this->update;
        }
    }

    /**
     * Test run method
     * @param string $token
     * @param string $chat_id
     */
    public function testing(string $token = '', $chat_id = '')
    {
        if (empty(trim($token)) && empty(trim($chat_id))) {
            $this->test = true;
            return;
        }

        if (! empty(trim($token))) {
            $this->token = $token;
        }

        if (! empty(trim($chat_id))) {
            $this->chat_id = $chat_id;
        }
    }

    /**
     * Returns the method url to the Telegram API
     * @return string
     */
    protected function compareApiMethod()
    {
        return $this->api_url . $this->token;
    }

    /**
     * Collects method parameters for the request
     * @param TelegramMethodsAbstract $method
     * @return array[]
     */
    protected function getParamsQuery(TelegramMethodsAbstract $method)
    {
        $query_params = [];

        foreach (array_keys( get_class_vars(get_class($method)) ) as $key)
        {
            if (!empty($method->$key))
            {
                $query_params[$key] = $method->$key;
            }
        }

        return $method->hasFiles()
            ? ['multipart' => $this->multipart($query_params, $method->hasFiles())]
            : ['form_params' => $query_params];
    }

    /**
     * Collect multipart request
     * @param array $query
     * @param array $files
     * @return array
     */
    private function multipart(array $query, array $files = [])
    {
        $query_params = [];
        foreach ($query as $key => $value) {
            if (is_array($value)) {
                $value = json_encode($value);
            }

            $query_params[] = [
                'name' => $key,
                'contents' => $value
            ];
        }

        return array_merge($query_params, $files);
    }

    /**
     * Sets up a test run
     * @param array $query_params
     */
    private function test(array $query_params)
    {
        if ($this->test !== false) {
            if (is_bool($this->test)) {
                dd($query_params);
            }

            if (is_array($this->test)) {
                $this->token = $this->test[0];
                $this->chat_id = $this->test[1];
            }
        }
    }
}