<?php

namespace tgbot\TelegramApi\Telegram\Methods;

use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;
use tgbot\TelegramApi\Telegram\Types\InputFile;

/**
 * Use this method to add a new sticker to a set created by the bot. Returns True on success.
 * @see https://core.telegram.org/bots/api#addstickertoset
 */
class AddStickerToSet extends TelegramMethodsAbstract
{
    /**
     * User identifier of sticker set owner
     * @var int
     */
    public $user_id = 0;

    /**
     * Sticker set name
     * @var string
     */
    public $name = '';

    /**
     * Png image with the sticker, must be up to 512 kilobytes in size,
     * dimensions must not exceed 512px, and either width or height must
     * be exactly 512px. Pass a file_id as a String to send a file that
     * already exists on the Telegram servers, pass an HTTP URL as a String
     * for Telegram to get a file from the Internet, or upload a new one
     * using multipart/form-data.
     * @var InputFile
     */
    public $png_sticker;

    /**
     * TGS animation with the sticker, uploaded using multipart/form-data.
     * See https://core.telegram.org/animated_stickers#technical-requirements
     * for technical requirements
     * @var InputFile
     */
    public $tgs_sticker;

    /**
     * One or more emoji corresponding to the sticker
     * @var string
     */
    public $emojis = '';

    /**
     * A JSON-serialized object for position where the mask should be placed on faces
     * @var string
     */
    public $mask_position;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['user_id', 'name', 'png_sticker', 'emojis'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return $data;
    }

    /**
     * @return void
     */
    public function beforeSending()
    {
        $this->png_sticker = $this->attachFile($this->png_sticker);
        $this->tgs_sticker = $this->attachFile($this->tgs_sticker);
    }
}