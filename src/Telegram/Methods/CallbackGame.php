<?php

namespace tgbot\TelegramApi\Telegram\Methods;

use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;

/**
 * A placeholder, currently holds no information. Use BotFather to set up your game.
 * @see https://core.telegram.org/bots/api#callbackgame
 */
class CallbackGame extends TelegramMethodsAbstract
{
    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return [];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return;
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}