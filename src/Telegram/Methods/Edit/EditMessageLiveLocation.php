<?php


namespace tgbot\TelegramApi\Telegram\Methods\Edit;


use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;
use tgbot\TelegramApi\Telegram\Types\Keyboards\Keyboards;
use tgbot\TelegramApi\Telegram\Types\Message;

/**
 * Use this method to edit live location messages. A location can be edited until
 * its live_period expires or editing is explicitly disabled by a call to
 * stopMessageLiveLocation. On success, if the edited message was sent by the bot,
 * the edited Message is returned, otherwise True is returned.
 * @package tgbot\TelegramApi\Methods
 * @see https://core.telegram.org/bots/api#editmessagelivelocation
 */
class EditMessageLiveLocation extends TelegramMethodsAbstract
{
    /**
     * Required if inline_message_id is not specified. Unique identifier for the
     * target chat or username of the target channel (in the format @channelusername)
     * @var int
     */
    public $chat_id = 0;

    /**
     * Required if inline_message_id is not specified. Identifier of the message to edit
     * @var int
     */
    public $message_id = 0;

    /**
     * Required if chat_id and message_id are not specified. Identifier of the inline message
     * @var int
     */
    public $inline_message_id = 0;

    /**
     * Latitude of new location
     * @var float
     */
    public $latitude = 0.00;

    /**
     * Longitude of new location
     * @var float
     */
    public $longitude = 0.00;

    /**
     * A JSON-serialized object for a new inline keyboard.
     * @var Keyboards
     */
    public $reply_markup;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array {
        return ['chat_id'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return is_bool($data) ? $data : new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}