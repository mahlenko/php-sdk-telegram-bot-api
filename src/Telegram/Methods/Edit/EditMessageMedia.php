<?php

namespace tgbot\TelegramApi\Telegram\Methods\Edit;

use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;
use tgbot\TelegramApi\Telegram\Types\InputMedia;
use tgbot\TelegramApi\Telegram\Types\Keyboards\InlineKeyboardMarkup;
use tgbot\TelegramApi\Telegram\Types\Message;

/**
 * Use this method to edit animation, audio, document, photo, or video messages.
 * If a message is a part of a message album, then it can be edited only to a photo or a video.
 * Otherwise, message type can be changed arbitrarily. When inline message is edited,
 * new file can't be uploaded. Use previously uploaded file via its file_id or specify a URL.
 * On success, if the edited message was sent by the bot, the edited Message is returned,
 * otherwise True is returned.
 * @package tgbot\TelegramApi\Methods
 */
class EditMessageMedia extends TelegramMethodsAbstract
{
    /**
     * Required if inline_message_id is not specified. Unique identifier for the
     * target chat or username of the target channel (in the format @channelusername)
     * @var int|string
     */
    public $chat_id = 0;

    /**
     * Required if inline_message_id is not specified.
     * Identifier of the message to edit
     * @var int
     */
    public $message_id = 0;

    /**
     * Required if chat_id and message_id are not specified.
     * Identifier of the inline message
     * @var string
     */
    public $inline_message_id = '';

    /**
     * A JSON-serialized object for a new media content of the message
     * @var InputMedia
     */
    public $media;

    /**
     * A JSON-serialized object for a new inline keyboard.
     * @var InlineKeyboardMarkup
     */
    public $reply_markup;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['media'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return is_bool($data) ? $data : new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {
        $this->media->media = $this->attachFile($this->media->media);
        $this->media = json_encode($this->media);
    }

}
