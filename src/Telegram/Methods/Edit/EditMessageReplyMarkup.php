<?php

namespace tgbot\TelegramApi\Telegram\Methods\Edit;

use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;
use tgbot\TelegramApi\Telegram\Types\Keyboards\InlineKeyboardMarkup;
use tgbot\TelegramApi\Telegram\Types\Message;

/**
 * Use this method to edit only the reply markup of messages. On success, if edited
 * message is sent by the bot, the edited Message is returned, otherwise True is returned.
 * @package tgbot\TelegramApi\Methods
 */
class EditMessageReplyMarkup extends TelegramMethodsAbstract
{
    /**
     * Required if inline_message_id is not specified. Unique identifier for the
     * target chat or username of the target channel (in the format @channelusername)
     * @var int|string
     */
    public $chat_id = 0;

    /**
     * Required if inline_message_id is not specified.
     * Identifier of the message to edit
     * @var int
     */
    public $message_id = 0;

    /**
     * Required if chat_id and message_id are not specified.
     * Identifier of the inline message
     * @var string
     */
    public $inline_message_id = '';

    /**
     * A JSON-serialized object for a new inline keyboard.
     * @var InlineKeyboardMarkup
     */
    public $reply_markup;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return [];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return is_bool($data) ? $data : new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}
