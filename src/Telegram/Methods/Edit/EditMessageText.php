<?php

namespace tgbot\TelegramApi\Telegram\Methods\Edit;

use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;
use tgbot\TelegramApi\Telegram\Types\Keyboards\InlineKeyboardMarkup;
use tgbot\TelegramApi\Telegram\Types\Message;

/**
 * Use this method to edit text and game messages. On success, if edited message
 * is sent by the bot, the edited Message is returned, otherwise True is returned.
 * @package tgbot\TelegramApi\Methods
 */
class EditMessageText extends TelegramMethodsAbstract
{
    /**
     * Required if inline_message_id is not specified. Unique identifier for the
     * target chat or username of the target channel (in the format @channelusername)
     * @var int|string
     */
    public $chat_id = 0;

    /**
     * Required if inline_message_id is not specified. Identifier of the message to edit
     * @var int
     */
    public $message_id = 0;

    /**
     * Required if chat_id and message_id are not specified.
     * Identifier of the inline message
     * @var string
     */
    public $inline_message_id = '';

    /**
     * New text of the message, 1-4096 characters after entities parsing
     * @var string
     */
    public $text = '';

    /**
     * Mode for parsing entities in the message text.
     * See formatting options for more details.
     * @var string
     */
    public $parse_mode = '';

    /**
     * Disables link previews for links in this message
     * @var bool
     */
    public $disable_web_page_preview = false;

    /**
     * A JSON-serialized object for an inline keyboard.
     * @var InlineKeyboardMarkup
     */
    public $reply_markup;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['text'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return is_bool($data) ? $data : new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}
