<?php


namespace tgbot\TelegramApi\Telegram\Methods;


use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;
use tgbot\TelegramApi\Telegram\Types\Message;

/**
 * Use this method to forward messages of any kind. On success, the sent Message is returned.
 * @see https://core.telegram.org/bots/api#forwardmessage
 */
class ForwardMessage extends TelegramMethodsAbstract
{
    /**
     * @var int
     */
    public $chat_id = 0;

    /**
     * @var int
     */
    public $from_chat_id = 0;

    /**
     * @var bool
     */
    public $disable_notification = false;

    /**
     * @var int
     */
    public $message_id = 0;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array {
        return ['chat_id', 'from_chat_id', 'message_id'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}