<?php

namespace tgbot\TelegramApi\Telegram\Methods\Get;

use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;
use tgbot\TelegramApi\Telegram\Types\ChatMember;

/**
 * Use this method to get a list of administrators in a chat.
 * On success, returns an Array of ChatMember objects that contains information
 * about all chat administrators except other bots. If the chat is a group or a
 * supergroup and no administrators were appointed, only the creator will be returned.
 * @package tgbot\TelegramApi\Methods
 */
class GetChatAdministrators extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat or username of the target supergroup
     * or channel (in the format @channelusername)
     * @var int|string
     */
    public $chat_id = 0;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['chat_id'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return $this->bindToObjectArray(ChatMember::class, $data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}
