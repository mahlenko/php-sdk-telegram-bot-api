<?php

namespace tgbot\TelegramApi\Telegram\Methods\Get;

use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;

/**
 * Use this method to get the number of members in a chat.
 * Returns Int on success.
 * @package tgbot\TelegramApi\Methods
 */
class GetChatMembersCount extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat or username of the target supergroup
     * or channel (in the format @channelusername)
     * @var int|string
     */
    public $chat_id = 0;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['chat_id'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return $data;
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}
