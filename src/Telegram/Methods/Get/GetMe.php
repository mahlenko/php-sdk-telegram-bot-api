<?php

namespace tgbot\TelegramApi\Telegram\Methods\Get;

use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;
use tgbot\TelegramApi\Telegram\Types\User;

/**
 * A simple method for testing your bot's auth token. Requires no parameters.
 * Returns basic information about the bot in form of a User object.
 * @package tgbot\TelegramApi\Methods
 */
class GetMe extends TelegramMethodsAbstract
{
    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return [];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new User($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}