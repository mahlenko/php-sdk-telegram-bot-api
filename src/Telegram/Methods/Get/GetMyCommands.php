<?php

namespace tgbot\TelegramApi\Telegram\Methods\Get;

use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;
use tgbot\TelegramApi\Telegram\Types\BotCommand;

/**
 * Use this method to get the current list of the bot's commands.
 * Requires no parameters. Returns Array of BotCommand on success.
 * @package tgbot\TelegramApi\Methods
 */
class GetMyCommands extends TelegramMethodsAbstract
{
    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return [];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return $this->bindToObjectArray(BotCommand::class, $data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}
