<?php

namespace tgbot\TelegramApi\Telegram\Methods\Get;

use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;
use tgbot\TelegramApi\Telegram\Types\UserProfilePhotos;

/**
 * Use this method to get a list of profile pictures for a user. Returns a
 * UserProfilePhotos object.
 * @package tgbot\TelegramApi\Methods
 * @see https://core.telegram.org/bots/api#getuserprofilephotos
 */
class GetUserProfilePhotos extends TelegramMethodsAbstract
{
    /**
     * Unique identifier of the target user
     * @var string
     */
    public $user_id = '';

    /**
     * Sequential number of the first photo to be returned. By default, all
     * photos are returned.
     * @var int
     */
    public $offset = 0;

    /**
     * Limits the number of photos to be retrieved. Values between 1—100 are accepted. Defaults to 100.
     * @var int
     */
    public $limit = 0;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['user_id'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new UserProfilePhotos($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}