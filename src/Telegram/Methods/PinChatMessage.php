<?php

namespace tgbot\TelegramApi\Telegram\Methods;

use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;

/**
 * Use this method to pin a message in a group, a supergroup, or a channel.
 * The bot must be an administrator in the chat for this to work and must have
 * the 'can_pin_messages' admin right in the supergroup or 'can_edit_messages'
 * admin right in the channel. Returns True on success.
 * @package tgbot\TelegramApi\Methods
 */
class PinChatMessage extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat or username of the target channel
     * (in the format @channelusername)
     * @var int|string
     */
    public $chat_id = 0;

    /**
     * Identifier of a message to pin
     * @var int
     */
    public $message_id = 0;

    /**
     * Pass True, if it is not necessary to send a notification to all chat
     * members about the new pinned message. Notifications are always disabled
     * in channels.
     * @var bool
     */
    public $disable_notification = false;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['chat_id', 'message_id'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return $data;
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}
