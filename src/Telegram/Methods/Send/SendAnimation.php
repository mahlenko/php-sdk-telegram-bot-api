<?php

namespace tgbot\TelegramApi\Telegram\Methods\Send;

use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;
use tgbot\TelegramApi\Telegram\Types\InputFile;
use tgbot\TelegramApi\Telegram\Types\Message;

/**
 * Use this method to send animation files (GIF or H.264/MPEG-4 AVC video
 * sound). On success, the sent Message is returned. Bots can currently send
 * animation files of up to 50 MB in size, this limit may be changed in the
 * future.
 * @see https://core.telegram.org/bots/api#sendanimation
 */
class SendAnimation extends TelegramMethodsAbstract
{
    /**
     * Animation to send. Pass a file_id as String to send an animation that exists
     * on the Telegram servers (recommended), pass an HTTP URL as a String for
     * Telegram to get an animation from the Internet, or upload a new animation
     * using multipart/form-data.
     * @var InputFile
     */
    public $animation;

    /**
     * Duration of sent animation in seconds
     * @var int
     */
    public $duration = 0;

    /**
     * Animation width
     * @var int
     */
    public $width = 0;

    /**
     * Animation height
     * @var int
     */
    public $height = 0;

    /**
     * Thumbnail of the file sent; can be ignored if thumbnail generation for
     * the file is supported server-side. The thumbnail should be in JPEG format
     * and less than 200 kB in size. A thumbnail‘s width and height should not
     * exceed 320. Ignored if the file is not uploaded using multipart/form-data.
     * Thumbnails can’t be reused and can be only uploaded as a new file, so you
     * can pass “attach://<file_attach_name>” if the thumbnail was uploaded using
     * multipart/form-data under <file_attach_name>.
     * @var InputFile
     */
    public $thumb;

    /**
     * Document caption (may also be used when resending documents by file_id),
     * 0-1024 characters after entities parsing
     * @var string
     */
    public $caption = '';

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array {
        return ['chat_id', 'animation'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {
        $this->animation = $this->attachFile($this->animation);
        $this->thumb = $this->attachFile($this->thumb);
    }
}