<?php

namespace tgbot\TelegramApi\Telegram\Methods\Send;

use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;
use tgbot\TelegramApi\Telegram\Types\InputFile;
use tgbot\TelegramApi\Telegram\Types\Message;

/**
 * Use this method to send audio files, if you want Telegram clients to display
 * them in the music player. Your audio must be in the .MP3 or .M4A format.
 * On success, the sent Message is returned. Bots can currently send audio files
 * of up to 50 MB in size, this limit may be changed in the future.
 *
 * For sending voice messages, use the sendVoice method instead.
 * @see https://core.telegram.org/bots/api#sendaudio
 */
class SendAudio extends TelegramMethodsAbstract
{
    /**
     * Audio file to send. Pass a file_id as String to send an audio file that
     * exists on the Telegram servers (recommended), pass an HTTP URL as a String
     * for Telegram to get an audio file from the Internet, or upload a new one
     * using multipart/form-data.
     * @var InputFile|String
     */
    public $audio;

    /**
     * Audio caption, 0-1024 characters after entities parsing
     * @var string
     */
    public $caption = '';

    /**
     * Duration of the audio in seconds
     * @var int
     */
    public $duration = 0;

    /**
     * Performer
     * @var string
     */
    public $performer = '';

    /**
     * Track name
     * @var string
     */
    public $title = '';

    /**
     * Thumbnail of the file sent; can be ignored if thumbnail generation for
     * the file is supported server-side. The thumbnail should be in JPEG format
     * and less than 200 kB in size. A thumbnail‘s width and height should not
     * exceed 320. Ignored if the file is not uploaded using multipart/form-data.
     * Thumbnails can’t be reused and can be only uploaded as a new file, so you
     * can pass “attach://<file_attach_name>” if the thumbnail was uploaded using
     * multipart/form-data under <file_attach_name>.
     * @var InputFile|string
     */
    public $thumb;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array {
        return ['chat_id', 'audio'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {
        $this->audio = $this->attachFile($this->audio);
        $this->thumb = $this->attachFile($this->thumb);
    }
}