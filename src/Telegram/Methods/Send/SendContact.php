<?php

namespace tgbot\TelegramApi\Telegram\Methods\Send;

use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;
use tgbot\TelegramApi\Telegram\Types\Keyboards\Keyboards;
use tgbot\TelegramApi\Telegram\Types\Message;

/**
 * Use this method to send phone contacts. On success, the sent Message is returned.
 * @package tgbot\TelegramApi\Methods
 * @see https://core.telegram.org/bots/api#sendcontact
 */
class SendContact extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat or username of the target channel
     * (in the format @channelusername)
     * @var int
     */
    public $chat_id = 0;

    /**
     * Contact's phone number
     * @var string
     */
    public $phone_number = '';

    /**
     * Contact's first name
     * @var string
     */
    public $first_name = '';

    /**
     * Contact's last name
     * @var string
     */
    public $last_name = '';

    /**
     * Additional data about the contact in the form of a vCard, 0-2048 bytes
     * @var string
     */
    public $vcard = '';

    /**
     * Sends the message silently. Users will receive a notification with no sound.
     * @var bool
     */
    public $disable_notification = false;

    /**
     * If the message is a reply, ID of the original message
     * @var int
     */
    public $reply_to_message_id = 0;

    /**
     * Additional interface options. A JSON-serialized object for an inline
     * keyboard, custom reply keyboard, instructions to remove keyboard or
     * to force a reply from the user.
     * @var Keyboards
     */
    public $reply_markup;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['chat_id', 'phone_number', 'first_name'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}