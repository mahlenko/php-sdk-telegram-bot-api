<?php

namespace tgbot\TelegramApi\Telegram\Methods\Send;

use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;
use tgbot\TelegramApi\Telegram\Types\InputFile;
use tgbot\TelegramApi\Telegram\Types\Keyboards\Keyboards;
use tgbot\TelegramApi\Telegram\Types\Message;

class SendDocument extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat or username of the target channel
     * (in the format @channelusername)
     * @var int
     */
    public $chat_id = 0;

    /**
     * File to send. Pass a file_id as String to send a file that exists on the
     * Telegram servers (recommended), pass an HTTP URL as a String for Telegram
     * to get a file from the Internet, or upload a new one using multipart/form-data.
     * @var InputFile|String
     */
    public $document;

    /**
     * Thumbnail of the file sent; can be ignored if thumbnail generation for
     * the file is supported server-side. The thumbnail should be in JPEG format
     * and less than 200 kB in size. A thumbnail‘s width and height should not
     * exceed 320. Ignored if the file is not uploaded using multipart/form-data.
     * Thumbnails can’t be reused and can be only uploaded as a new file, so you
     * can pass “attach://<file_attach_name>” if the thumbnail was uploaded using
     * multipart/form-data under <file_attach_name>.
     * @var InputFile|String
     */
    public $thumb;

    /**
     * Document caption (may also be used when resending documents by file_id),
     * 0-1024 characters after entities parsing
     * @var string
     */
    public $caption = '';

    /**
     * Mode for parsing entities in the message text. See formatting options for more details.
     * @var string
     */
    public $parse_mode = '';

    /**
     * Sends the message silently. Users will receive a notification with no sound.
     * @var bool
     */
    public $disable_notification = false;

    /**
     * If the message is a reply, ID of the original message
     * @var int
     */
    public $reply_to_message_id = 0;

    /**
     * Additional interface options. A JSON-serialized object for an inline keyboard,
     * custom reply keyboard, instructions to remove reply keyboard or to force a
     * reply from the user.
     * @var Keyboards
     */
    public $reply_markup;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array {
        return ['chat_id', 'document'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {
        $this->document = $this->attachFile($this->document);
        $this->thumb = $this->attachFile($this->thumb);
    }
}