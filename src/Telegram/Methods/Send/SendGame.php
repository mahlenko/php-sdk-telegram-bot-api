<?php

namespace tgbot\TelegramApi\Telegram\Methods\Send;

use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;
use tgbot\TelegramApi\Telegram\Types\Keyboards\InlineKeyboardMarkup;
use tgbot\TelegramApi\Telegram\Types\Message;

/**
 * Use this method to send a game. On success, the sent Message is returned.
 * @see https://core.telegram.org/bots/api#sendgame
 */
class SendGame extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat
     * @var int
     */
    public $chat_id = 0;

    /**
     * @var string
     */
    public $game_short_name = '';

    /**
     * Short name of the game, serves as the unique identifier for the game.
     * Set up your games via Botfather.
     * @var bool
     */
    public $disable_notification = false;

    /**
     * Sends the message silently. Users will receive a notification with no sound.
     * @var int
     */
    public $reply_to_message_id = 0;

    /**
     * If the message is a reply, ID of the original message
     * @var InlineKeyboardMarkup
     */
    public $reply_markup;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['chat_id', 'game_short_name'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}