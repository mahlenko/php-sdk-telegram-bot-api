<?php

namespace tgbot\TelegramApi\Telegram\Methods\Send;

use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;
use tgbot\TelegramApi\Telegram\Types\Keyboards\Keyboards;
use tgbot\TelegramApi\Telegram\Types\Message;
use tgbot\TelegramApi\Telegram\Types\Payments\LabeledPrice;

/**
 * Use this method to send invoices. On success, the sent Message is returned.
 * @see https://core.telegram.org/bots/api#sendinvoice
 */
class SendInvoice extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target private chat
     * @var int
     */
    public $chat_id = 0;

    /**
     * Product name, 1-32 characters
     * @var string
     */
    public $title = '';

    /**
     * Product description, 1-255 characters
     * @var string
     */
    public $description = '';

    /**
     * Bot-defined invoice payload, 1-128 bytes. This will not be displayed to
     * the user, use for your internal processes.
     * @var string
     */
    public $payload = '';

    /**
     * Payments provider token, obtained via Botfather
     * @var string
     */
    public $provider_token = '';

    /**
     * Unique deep-linking parameter that can be used to generate
     * this invoice when used as a start parameter
     * @var string
     */
    public $start_parameter = '';

    /**
     * Three-letter ISO 4217 currency code, see more on currencies
     * @var string
     */
    public $currency = '';

    /**
     * Price breakdown, a JSON-serialized list of components (e.g. product price,
     * tax, discount, delivery cost, delivery tax, bonus, etc.)
     * @var LabeledPrice[]
     */
    public $prices = [];

    /**
     * JSON-encoded data about the invoice, which will be shared with
     * the payment provider. A detailed description of required fields should
     * be provided by the payment provider.
     * @var string
     */
    public $provider_data = '';

    /**
     * URL of the product photo for the invoice. Can be a photo of the goods or
     * a marketing image for a service. People like it better when they see what
     * they are paying for.
     * @var string
     */
    public $photo_url = '';

    /**
     * Photo size
     * @var int
     */
    public $photo_size = 0;

    /**
     * Photo width
     * @var int
     */
    public $photo_width = 0;

    /**
     * Photo height
     * @var int
     */
    public $photo_height = 0;

    /**
     * Pass True, if you require the user's full name to complete the order
     * @var bool
     */
    public $need_name = false;

    /**
     * Pass True, if you require the user's phone number to complete the order
     * @var bool
     */
    public $need_phone_number = false;

    /**
     * Pass True, if you require the user's email address to complete the order
     * @var bool
     */
    public $need_email = false;

    /**
     * Pass True, if you require the user's shipping address to complete the order
     * @var bool
     */
    public $need_shipping_address = false;

    /**
     * Pass True, if user's phone number should be sent to provider
     * @var bool
     */
    public $send_phone_number_to_provider = false;

    /**
     * Pass True, if user's email address should be sent to provider
     * @var bool
     */
    public $send_email_to_provider = false;

    /**
     * Pass True, if the final price depends on the shipping method
     * @var bool
     */
    public $is_flexible = false;

    /**
     * Sends the message silently. Users will receive a notification with no sound.
     * @var bool
     */
    public $disable_notification = false;

    /**
     * If the message is a reply, ID of the original message
     * @var int
     */
    public $reply_to_message_id = 0;

    /**
     * A JSON-serialized object for an inline keyboard. If empty,
     * one 'Pay total price' button will be shown. If not empty, the first
     * button must be a Pay button.
     * @var Keyboards
     */
    public $reply_markup;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['chat_id', 'title', 'description', 'payload', 'provider_token', 'start_parameter', 'currency', 'prices'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}