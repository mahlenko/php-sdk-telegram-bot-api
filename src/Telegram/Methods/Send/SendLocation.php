<?php

namespace tgbot\TelegramApi\Telegram\Methods\Send;

use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;
use tgbot\TelegramApi\Telegram\Types\Keyboards\Keyboards;
use tgbot\TelegramApi\Telegram\Types\Message;

/**
 * Use this method to send point on the map. On success, the sent Message is returned.
 * @package tgbot\TelegramApi\Methods
 * @see https://core.telegram.org/bots/api#sendlocation
 */
class SendLocation extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat or username of the target channel
     * (in the format @channelusername)
     * @var int
     */
    public $chat_id = 0;

    /**
     * Latitude of the location
     * @var float
     */
    public $latitude = 0.0;

    /**
     * Longitude of the location
     * @var float
     */
    public $longitude = 0.0;

    /**
     * Period in seconds for which the location will be updated (see Live
     * Locations, should be between 60 and 86400.
     * @var int
     */
    public $live_period = 0;

    /**
     * Sends the message silently. Users will receive a notification with no sound.
     * @var bool
     */
    public $disable_notification = false;

    /**
     * If the message is a reply, ID of the original message
     * @var int
     */
    public $reply_to_message_id = 0;

    /**
     * Additional interface options. A JSON-serialized object for an inline
     * keyboard, custom reply keyboard, instructions to remove reply keyboard
     * or to force a reply from the user.
     * @var Keyboards
     */
    public $reply_markup;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['chat_id', 'latitude', 'longitude'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}