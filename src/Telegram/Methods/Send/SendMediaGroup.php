<?php

namespace tgbot\TelegramApi\Telegram\Methods\Send;

use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;
use tgbot\TelegramApi\Telegram\Types\Message;

/**
 * Use this method to send a group of photos or videos as an album.
 * On success, an array of the sent Messages is returned.
 * @see https://core.telegram.org/bots/api#sendmediagroup
 */
class SendMediaGroup extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat or username of the target channel
     * (in the format @channelusername)
     * @var int
     */
    public $chat_id = 0;

    /**
     * A JSON-serialized array describing photos and videos to be sent,
     * must include 2–10 items
     * @var array
     */
    public $media = [];

    /**
     * Sends the messages silently. Users will receive a notification with no sound.
     * @var bool
     */
    public $disable_notification = false;

    /**
     * If the messages are a reply, ID of the original message
     * @var int
     */
    public $reply_to_message_id = 0;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['chat_id', 'media'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return $this->bindToObjectArray(Message::class, $data);
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function beforeSending()
    {
        foreach ($this->media as $index => $file) {
            $this->media[$index]->media = $this->attachFile($file->media);
        }
    }
}