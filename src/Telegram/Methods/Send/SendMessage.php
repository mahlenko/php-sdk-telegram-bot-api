<?php

namespace tgbot\TelegramApi\Telegram\Methods\Send;

use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;
use tgbot\TelegramApi\Telegram\Types\Keyboards\Keyboards;
use tgbot\TelegramApi\Telegram\Types\Message;

/**
 * Use this method to send text messages. On success, the sent Message is returned.
 * @see https://core.telegram.org/bots/api#sendmessage
 */
class SendMessage extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat or username of the target channel
     * (in the format @channelusername)
     * @var int
     */
    public $chat_id = 0;

    /**
     * Text of the message to be sent, 1-4096 characters after entities parsing
     * @var string
     */
    public $text = '';

    /**
     * Mode for parsing entities in the message text. See formatting options for more details.
     * @var string
     */
    public $parse_mode = '';

    /**
     * Disables link previews for links in this message
     * @var bool
     */
    public $disable_web_page_preview = false;

    /**
     * Sends the message silently. Users will receive a notification with no sound.
     * @var bool
     */
    public $disable_notification = false;

    /**
     * If the message is a reply, ID of the original message
     * @var int
     */
    public $reply_to_message_id = 0;

    /**
     * Additional interface options. A JSON-serialized object for an inline keyboard,
     * custom reply keyboard, instructions to remove reply keyboard or to force a
     * reply from the user.
     * @var Keyboards
     */
    public $reply_markup;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array {
        return ['chat_id', 'text'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new Message($data);
    }


    /**
     * @return void
     */
    public function beforeSending()
    {
        if (!empty($this->reply_markup) && !is_string($this->reply_markup)) {
            $this->reply_markup = (array) $this->reply_markup;
            $this->reply_markup = json_encode(array_filter($this->reply_markup));
        }
    }
}