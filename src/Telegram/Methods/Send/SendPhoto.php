<?php

namespace tgbot\TelegramApi\Telegram\Methods\Send;

use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;
use tgbot\TelegramApi\Telegram\Types\InputFile;
use tgbot\TelegramApi\Telegram\Types\Keyboards\Keyboards;
use tgbot\TelegramApi\Telegram\Types\Message;

/**
 * Use this method to send photos. On success, the sent Message is returned.
 * @see https://core.telegram.org/bots/api#sendphoto
 */
class SendPhoto extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat or username of the target channel
     * (in the format @channelusername)
     * @var int
     */
    public $chat_id = 0;

    /**
     * Photo to send. Pass a file_id as String to send a photo that exists on
     * the Telegram servers (recommended), pass an HTTP URL as a String
     * for Telegram to get a photo from the Internet, or upload a new photo
     * using multipart/form-data.
     * @see https://core.telegram.org/bots/api#sending-files
     * @var InputFile|String
     */
    public $photo;

    /**
     * Photo caption (may also be used when resending photos by file_id),
     * 0-1024 characters after entities parsing
     * @var string
     */
    public $caption = '';

    /**
     * Mode for parsing entities in the message text. See formatting options for more details.
     * @var string
     */
    public $parse_mode = '';

    /**
     * Sends the message silently. Users will receive a notification with no sound.
     * @var bool
     */
    public $disable_notification = false;

    /**
     * If the message is a reply, ID of the original message
     * @var int
     */
    public $reply_to_message_id = 0;

    /**
     * Additional interface options. A JSON-serialized object for an inline keyboard,
     * custom reply keyboard, instructions to remove reply keyboard or to force a
     * reply from the user.
     * @var Keyboards
     */
    public $reply_markup;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array {
        return ['chat_id', 'photo'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {
        $this->photo = $this->attachFile($this->photo);
    }
}