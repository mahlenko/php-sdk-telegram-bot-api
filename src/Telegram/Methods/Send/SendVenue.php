<?php

namespace tgbot\TelegramApi\Telegram\Methods\Send;

use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;
use tgbot\TelegramApi\Telegram\Types\Keyboards\Keyboards;
use tgbot\TelegramApi\Telegram\Types\Message;

/**
 * Use this method to send information about a venue. On success, the sent
 * Message is returned.
 * @package tgbot\TelegramApi\Methods
 * @see https://core.telegram.org/bots/api#sendvenue
 */
class SendVenue extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat or username of the target channel
     * (in the format @channelusername)
     * @var int
     */
    public $chat_id	= 0;

    /**
     * Latitude of the venue
     * @var float
     */
    public $latitude = 0.00;

    /**
     * Longitude of the venue
     * @var float
     */
    public $longitude = 0.00;

    /**
     * Name of the venue
     * @var string
     */
    public $title = '';

    /**
     * Address of the venue
     * @var string
     */
    public $address = '';

    /**
     * Foursquare identifier of the venue
     * @var string
     */
    public $foursquare_id = '';

    /**
     * Foursquare type of the venue, if known. (For example,
     * “arts_entertainment/default”, “arts_entertainment/aquarium”
     * or “food/icecream”.)
     * @var string
     */
    public $foursquare_type = '';

    /**
     * Sends the message silently. Users will receive a notification with no sound.
     * @var bool
     */
    public $disable_notification = false;

    /**
     * If the message is a reply, ID of the original message
     * @var int
     */
    public $reply_to_message_id = 0;

    /**
     * Additional interface options. A JSON-serialized object for an
     * inline keyboard, custom reply keyboard, instructions to remove
     * reply keyboard or to force a reply from the user.
     * @var Keyboards
     */
    public $reply_markup;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['chat_id', 'latitude', 'longitude', 'title', 'address'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}