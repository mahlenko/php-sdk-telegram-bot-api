<?php

namespace tgbot\TelegramApi\Telegram\Methods\Set;

use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;
use tgbot\TelegramApi\Telegram\Types\BotCommand;

/**
 * Use this method to change the list of the bot's commands.
 * Returns True on success.
 * @package tgbot\TelegramApi\Methods
 */
class SetMyCommands extends TelegramMethodsAbstract
{
    /**
     * A JSON-serialized list of bot commands to be set as the list of the bot's commands.
     * At most 100 commands can be specified.
     * @var array|BotCommand
     */
    public $commands = [];

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['commands'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return $data;
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}
