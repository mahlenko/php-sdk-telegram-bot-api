<?php

namespace tgbot\TelegramApi\Telegram\Methods\Set;

use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;
use tgbot\TelegramApi\Telegram\Types\InputFile;

/**
 * Use this method to specify a url and receive incoming updates via an outgoing webhook.
 * Whenever there is an update for the bot, we will send an HTTPS POST request to
 * the specified url, containing a JSON-serialized Update. In case of an unsuccessful
 * request, we will give up after a reasonable amount of attempts.
 * Returns True on success.
 *
 * If you'd like to make sure that the Webhook request comes from Telegram, we
 * recommend using a secret path in the URL, e.g. https://www.example.com/<token>.
 * Since nobody else knows your bot's token, you can be pretty sure it's us.
 * @package tgbot\TelegramApi\Methods
 */
class SetWebhook extends TelegramMethodsAbstract
{
    /**
     * HTTPS url to send updates to.
     * Use an empty string to remove webhook integration
     * @var string
     */
    public $url = '';

    /**
     * Upload your public key certificate so that the root certificate in use
     * can be checked. See our self-signed guide for details.
     * @var string
     */
    public $certificate = '';

    /**
     * Maximum allowed number of simultaneous HTTPS connections to the webhook
     * for update delivery, 1-100. Defaults to 40. Use lower values to limit the
     * load on your bot's server, and higher values to increase your bot's throughput.
     * @var string
     */
    public $max_connections = '';

    /**
     * A JSON-serialized list of the update types you want your bot to receive. 
     * For example, specify ["message", "edited_channel_post", "callback_query"] 
     * to only receive updates of these types. See Update for a complete list of 
     * available update types. Specify an empty list to receive all updates 
     * regardless of type (default). If not specified, the previous setting 
     * will be used.
     * @var array
     */
    public $allowed_updates = [];

    /**
     * @inheritDoc
     */
    public function requiredFields(): array
    {
        return ['url'];
    }

    /**
     * @inheritDoc
     */
    public function bindToObject($data)
    {
        return $data;
    }

    /**
     * @return void
     */
    public function beforeSending()
    {
        if (! empty($this->certificate)) {
            if (! $this->certificate instanceof InputFile) {
                $this->certificate = new InputFile($this->certificate);
            }

            $this->local_files[] = $this->certificate->file;
            $this->certificate = $this->certificate->url;
        }
    }
}