<?php

namespace tgbot\TelegramApi\Telegram\Methods\Stop;

use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;
use tgbot\TelegramApi\Telegram\Types\Keyboards\InlineKeyboardMarkup;
use tgbot\TelegramApi\Telegram\Types\Message;

/**
 * Use this method to stop updating a live location message before live_period
 * expires. On success, if the message was sent by the bot, the sent Message is
 * returned, otherwise True is returned.
 * @package tgbot\TelegramApi\Methods
 * @see https://core.telegram.org/bots/api#stopmessagelivelocation
 */
class StopMessageLiveLocation extends TelegramMethodsAbstract
{
    /**
     * Required if inline_message_id is not specified. Unique identifier for the
     * target chat or username of the target channel (in the format @channelusername)
     * @var int
     */
    public $chat_id = 0;

    /**
     * Required if inline_message_id is not specified. Identifier of the message
     * with live location to stop
     * @var int
     */
    public $message_id = 0;

    /**
     * Required if chat_id and message_id are not specified. Identifier of the inline message
     * @var string
     */
    public $inline_message_id = '';

    /**
     * A JSON-serialized object for a new inline keyboard.
     * @var InlineKeyboardMarkup
     */
    public $reply_markup;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return [];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}