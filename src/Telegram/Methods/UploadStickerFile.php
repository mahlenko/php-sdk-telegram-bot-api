<?php


namespace tgbot\TelegramApi\Telegram\Methods;

use tgbot\TelegramApi\Abstracts\TelegramMethodsAbstract;
use tgbot\TelegramApi\Telegram\Types\File;
use tgbot\TelegramApi\Telegram\Types\InputFile;

/**
 * Use this method to upload a .png file with a sticker for later use in
 * createNewStickerSet and addStickerToSet methods (can be used multiple times).
 * Returns the uploaded File on success.
 * @see https://core.telegram.org/bots/api#uploadstickerfile
 */
class UploadStickerFile extends TelegramMethodsAbstract
{
    /**
     * User identifier of sticker file owner
     * @var int
     */
    public $user_id = 0;

    /**
     * Png image with the sticker, must be up to 512 kilobytes in size,
     * dimensions must not exceed 512px, and either width or height must be
     * exactly 512px.
     * @var InputFile
     */
    public $png_sticker;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['user_id', 'png_sticker'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new File($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {
        $this->png_sticker = $this->attachFile($this->png_sticker);
    }
}