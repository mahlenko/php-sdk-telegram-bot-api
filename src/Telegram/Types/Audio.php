<?php

namespace tgbot\TelegramApi\Telegram\Types;

use tgbot\TelegramApi\Abstracts\TelegramTypesAbstract;


class Audio extends TelegramTypesAbstract
{
    /**
     * Identifier for this file, which can be used to download or reuse the file
     * @var string
     */
    public $file_id = '';

    /**
     * Unique identifier for this file, which is supposed to be the same over
     * time and for different bots. Can't be used to download or reuse the file.
     * @var string
     */
    public $file_unique_id = '';

    /**
     * Duration of the audio in seconds as defined by sender
     * @var int
     */
    public $duration = 0;

    /**
     * Optional. Performer of the audio as defined by sender or by audio tags
     * @var string
     */
    public $performer = '';

    /**
     * Optional. Title of the audio as defined by sender or by audio tags
     * @var string
     */
    public $title = '';

    /**
     * Optional. MIME type of the file as defined by sender
     * @var string
     */
    public $mime_type = '';

    /**
     * Optional. File size
     * @var int
     */
    public $file_size = 0;

    /**
     * Optional. Thumbnail of the album cover to which the music file belongs
     * @var PhotoSize
     */
    public $thumb;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            PhotoSize::class => 'thumb'
        ];
    }
}