<?php

namespace tgbot\TelegramApi\Telegram\Types;

use tgbot\TelegramApi\Abstracts\TelegramTypesAbstract;

/**
 * This object represents a bot command.
 * @package tgbot\TelegramApi\Types
 */
class BotCommand extends TelegramTypesAbstract
{
    /**
     * Text of the command, 1-32 characters. Can contain only lowercase English
     * letters, digits and underscores.
     * @var string
     */
    public $command = '';

    /**
     * Description of the command, 3-256 characters.
     * @var string
     */
    public $description = '';

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}
