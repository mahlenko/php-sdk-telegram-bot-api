<?php

namespace tgbot\TelegramApi\Telegram\Types;

use tgbot\TelegramApi\Abstracts\TelegramTypesAbstract;

/**
 * This object contains information about one member of a chat.
 * @see https://core.telegram.org/bots/api#chatmember
 */
class ChatMember extends TelegramTypesAbstract
{
    /**
     * Information about the user
     * @var User
     */
    public $user;

    /**
     * The member's status in the chat. Can be “creator”, “administrator”,
     * “member”, “restricted”, “left” or “kicked”
     * @var string
     */
    public $status = '';

    /**
     * Optional. Owner and administrators only. Custom title for this user
     * @var string
     */
    public $custom_title = '';

    /**
     * Optional. Restricted and kicked only. Date when restrictions will be
     * lifted for this user; unix time
     * @var int
     */
    public $until_date = 0;

    /**
     * Optional. Administrators only. True, if the bot is allowed to
     * edit administrator privileges of that user
     * @var bool
     */
    public $can_be_edited = false;

    /**
     * Optional. Administrators only. True, if the administrator can post in the
     * channel; channels only
     * @var bool
     */
    public $can_post_messages = false;

    /**
     * Optional. Administrators only. True, if the administrator can edit messages
     * of other users and can pin messages; channels only
     * @var bool
     */
    public $can_edit_messages = false;

    /**
     * Optional. Administrators only. True, if the administrator can delete
     * messages of other users
     * @var bool
     */
    public $can_delete_messages = false;

    /**
     * Optional. Administrators only. True, if the administrator can restrict,
     * ban or unban chat members
     * @var bool
     */
    public $can_restrict_members = false;

    /**
     * Optional. Administrators only. True, if the administrator can add new
     * administrators with a subset of his own privileges or demote administrators
     * that he has promoted, directly or indirectly (promoted by administrators
     * that were appointed by the user)
     * @var bool
     */
    public $can_promote_members = false;

    /**
     * Optional. Administrators and restricted only. True, if the user is allowed
     * to change the chat title, photo and other settings
     * @var bool
     */
    public $can_change_info = false;

    /**
     * Optional. Administrators and restricted only. True, if the user is allowed
     * to invite new users to the chat
     * @var bool
     */
    public $can_invite_users = false;

    /**
     * Optional. Administrators and restricted only. True, if the user is allowed
     * to pin messages; groups and supergroups only
     * @var bool
     */
    public $can_pin_messages = false;

    /**
     * Optional. Restricted only. True, if the user is a member of the chat at
     * the moment of the request
     * @var bool
     */
    public $is_member = false;

    /**
     * Optional. Restricted only. True, if the user is allowed to send text messages,
     * contacts, locations and venues
     * @var bool
     */
    public $can_send_messages = false;

    /**
     * Optional. Restricted only. True, if the user is allowed to send audios,
     * documents, photos, videos, video notes and voice notes
     * @var bool
     */
    public $can_send_media_messages = false;

    /**
     * Optional. Restricted only. True, if the user is allowed to send polls
     * @var bool
     */
    public $can_send_polls = false;

    /**
     * Optional. Restricted only. True, if the user is allowed to send animations,
     * games, stickers and use inline bots
     * @var bool
     */
    public $can_send_other_messages = false;

    /**
     * Optional. Restricted only. True, if the user is allowed to add web page
     * previews to their messages
     * @var bool
     */
    public $can_add_web_page_previews = false;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            User::class => 'user'
        ];
    }
}