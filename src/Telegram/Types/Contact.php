<?php

namespace tgbot\TelegramApi\Telegram\Types;

use tgbot\TelegramApi\Abstracts\TelegramTypesAbstract;

/**
 * This object represents a phone contact.
 * @see https://core.telegram.org/bots/api#contact
 */
class Contact extends TelegramTypesAbstract
{
    /**
     * Contact's phone number
     * @var string
     */
    public $phone_number = '';

    /**
     * Contact's first name
     * @var string
     */
    public $first_name = '';

    /**
     * Optional. Contact's last name
     * @var string
     */
    public $last_name = '';

    /**
     * Optional. Contact's user identifier in Telegram
     * @var int
     */
    public $user_id = 0;

    /**
     * Optional. Additional data about the contact in the form of a vCard
     * @var string
     */
    public $vcard = '';

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}