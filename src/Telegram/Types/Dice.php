<?php

namespace tgbot\TelegramApi\Telegram\Types;

use tgbot\TelegramApi\Abstracts\TelegramTypesAbstract;

/**
 * This object represents a dice with a random value from 1 to 6 for currently
 * supported base emoji. (Yes, we're aware of the “proper” singular of die.
 * But it's awkward, and we decided to help it change. One dice at a time!)
 * @package tgbot\TelegramApi\Types
 */
class Dice extends TelegramTypesAbstract
{
    /**
     * Emoji on which the dice throw animation is based
     * @var string
     */
    public $emoji = '';

    /**
     * Value of the dice, 1-6 for currently supported base emoji
     * @var int
     */
    public $value = 0;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}