<?php

namespace tgbot\TelegramApi\Telegram\Types;

use tgbot\TelegramApi\Abstracts\TelegramTypesAbstract;


/**
 * This object represents a general file
 * (as opposed to photos, voice messages and audio files).
 * @see https://core.telegram.org/bots/api#document
 */
class Document extends TelegramTypesAbstract
{
    /**
     * Identifier for this file, which can be used to download or reuse the file
     * @var string
     */
    public $file_id = '';

    /**
     * Unique identifier for this file, which is supposed to be the same over
     * time and for different bots. Can't be used to download or reuse the file.
     * @var string
     */
    public $file_unique_id = '';

    /**
     * Optional. Document thumbnail as defined by sender
     * @var PhotoSize
     */
    public $thumb;

    /**
     * Optional. Original filename as defined by sender
     * @var string
     */
    public $file_name = '';

    /**
     * Optional. MIME type of the file as defined by sender
     * @var string
     */
    public $mime_type = '';

    /**
     * Optional. File size
     * @var int
     */
    public $file_size = 0;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            PhotoSize::class => 'thumb'
        ];
    }
}