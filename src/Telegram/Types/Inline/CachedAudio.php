<?php

namespace tgbot\TelegramApi\Telegram\Types\Inline;

use tgbot\TelegramApi\Telegram\Types\InlineQueryResult;
use tgbot\TelegramApi\Telegram\Types\InputMessageContent;
use tgbot\TelegramApi\Telegram\Types\Keyboards\InlineKeyboardMarkup;

/**
 * Represents a link to an MP3 audio file stored on the Telegram servers.
 * By default, this audio file will be sent by the user. Alternatively,
 * you can use input_message_content to send a message with the specified
 * content instead of the audio.
 *
 * | Note: This will only work in Telegram versions released after 9 April, 2016.
 * | Older clients will ignore them.
 *
 * @package tgbot\TelegramApi\Types\Inline
 * @see https://core.telegram.org/bots/api#inlinequeryresultcachedaudio
 */
class CachedAudio extends InlineQueryResult
{
    /**
     * Type of the result, must be audio
     * @var string
     */
    public $type = 'audio';

    /**
     * Unique identifier for this result, 1-64 bytes
     * @var string
     */
    public $id = '';

    /**
     * A valid file identifier for the audio file
     * @var string
     */
    public $audio_file_id = '';

    /**
     * Optional. Caption, 0-1024 characters after entities parsing
     * @var string
     */
    public $caption = '';

    /**
     * Optional. Send Markdown or HTML, if you want Telegram apps to show bold,
     * italic, fixed-width text or inline URLs in the media caption.
     * @var string
     */
    public $parse_mode = '';

    /**
     * Optional. Inline keyboard attached to the message
     * @var InlineKeyboardMarkup
     */
    public $reply_markup;

    /**
     * Optional. Content of the message to be sent instead of the audio
     * @var InputMessageContent
     */
    public $input_message_content;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            InputMessageContent::class  => 'input_message_content',
            InlineKeyboardMarkup::class => 'reply_markup'
        ];
    }
}