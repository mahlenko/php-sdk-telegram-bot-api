<?php


namespace tgbot\TelegramApi\Telegram\Types\Inline;

use tgbot\TelegramApi\Telegram\Types\InlineQueryResult;
use tgbot\TelegramApi\Telegram\Types\InputMessageContent;
use tgbot\TelegramApi\Telegram\Types\Keyboards\InlineKeyboardMarkup;

/**
 * Represents a link to a video animation (H.264/MPEG-4 AVC video without sound)
 * stored on the Telegram servers. By default, this animated MPEG-4 file will
 * be sent by the user with an optional caption. Alternatively, you can use
 * input_message_content to send a message with the specified content instead
 * of the animation.
 * @package tgbot\TelegramApi\Types\Inline
 * @see https://core.telegram.org/bots/api#inlinequeryresultcachedmpeg4gif
 */
class CachedMpeg4Gif extends InlineQueryResult
{
    /**
     * Type of the result, must be mpeg4_gif
     * @var string
     */
    public $type = 'mpeg4_gif';

    /**
     * Unique identifier for this result, 1-64 bytes
     * @var string
     */
    public $id = '';

    /**
     * A valid file identifier for the MP4 file
     * @var string
     */
    public $mpeg4_file_id = '';

    /**
     * Optional. Title for the result
     * @var string
     */
    public $title = '';

    /**
     * Optional. Caption of the MPEG-4 file to be sent, 0-1024 characters
     * after entities parsing
     * @var string
     */
    public $caption = '';

    /**
     * Optional. Send Markdown or HTML, if you want Telegram apps to show bold,
     * italic, fixed-width text or inline URLs in the media caption.
     * @var string
     */
    public $parse_mode = '';

    /**
     * Optional. Inline keyboard attached to the message
     * @var InlineKeyboardMarkup
     */
    public $reply_markup;

    /**
     * Optional. Content of the message to be sent instead of the video animation
     * @var InputMessageContent
     */
    public $input_message_content;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            InputMessageContent::class  => 'input_message_content',
            InlineKeyboardMarkup::class => 'reply_markup'
        ];
    }
}