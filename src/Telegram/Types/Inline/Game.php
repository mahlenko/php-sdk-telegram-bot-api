<?php

namespace tgbot\TelegramApi\Telegram\Types\Inline;

use tgbot\TelegramApi\Telegram\Types\InlineQueryResult;
use tgbot\TelegramApi\Telegram\Types\Keyboards\InlineKeyboardMarkup;

/**
 * Represents a Game.
 *
 * | Note: This will only work in Telegram versions released after October 1, 2016.
 * | Older clients will not display any inline results if a game result is among them.
 *
 * @package tgbot\TelegramApi\Types\Inline
 * @see https://core.telegram.org/bots/api#inlinequeryresultgame
 */
class Game extends InlineQueryResult
{
    /**
     * Type of the result, must be game
     * @var string
     */
    public $type = 'game';

    /**
     * Unique identifier for this result, 1-64 bytes
     * @var string
     */
    public $id = '';

    /**
     * Short name of the game
     * @var string
     */
    public $game_short_name = '';

    /**
     * Optional. Inline keyboard attached to the message
     * @var InlineKeyboardMarkup
     */
    public $reply_markup;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            InlineKeyboardMarkup::class => 'reply_markup'
        ];
    }
}