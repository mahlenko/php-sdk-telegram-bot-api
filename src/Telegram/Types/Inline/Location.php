<?php

namespace tgbot\TelegramApi\Telegram\Types\Inline;

use tgbot\TelegramApi\Telegram\Types\InlineQueryResult;
use tgbot\TelegramApi\Telegram\Types\InputMessageContent;
use tgbot\TelegramApi\Telegram\Types\Keyboards\InlineKeyboardMarkup;

/**
 * Represents a location on a map. By default, the location will be sent by the
 * user. Alternatively, you can use input_message_content to send a message with
 * the specified content instead of the location.
 *
 * | Note: This will only work in Telegram versions released after 9 April, 2016.
 * | Older clients will ignore them.
 *
 * @package tgbot\TelegramApi\Types\Inline
 * @see https://core.telegram.org/bots/api#inlinequeryresultlocation
 */
class Location extends InlineQueryResult
{
    /**
     * Type of the result, must be location
     * @var string
     */
    public $type = 'location';

    /**
     * Unique identifier for this result, 1-64 Bytes
     * @var string
     */
    public $id = '';

    /**
     * Location latitude in degrees
     * @var float
     */
    public $latitude = 0.0;

    /**
     * Location longitude in degrees
     * @var float
     */
    public $longitude = 0.0;

    /**
     * Location title
     * @var string
     */
    public $title = '';

    /**
     * Optional. Period in seconds for which the location can be updated,
     * should be between 60 and 86400.
     * @var int
     */
    public $live_period = 0;

    /**
     * Optional. Inline keyboard attached to the message
     * @var InlineKeyboardMarkup
     */
    public $reply_markup;

    /**
     * Optional. Content of the message to be sent instead of the location
     * @var InputMessageContent
     */
    public $input_message_content;

    /**
     * Optional. Url of the thumbnail for the result
     * @var string
     */
    public $thumb_url = '';

    /**
     * Optional. Thumbnail width
     * @var int
     */
    public $thumb_width = 0;

    /**
     * Optional. Thumbnail height
     * @var int
     */
    public $thumb_height = 0;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            InputMessageContent::class  => 'input_message_content',
            InlineKeyboardMarkup::class => 'reply_markup'
        ];
    }
}