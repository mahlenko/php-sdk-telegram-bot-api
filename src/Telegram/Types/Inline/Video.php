<?php

namespace tgbot\TelegramApi\Telegram\Types\Inline;

use tgbot\TelegramApi\Telegram\Types\InlineQueryResult;
use tgbot\TelegramApi\Telegram\Types\InputMessageContent;
use tgbot\TelegramApi\Telegram\Types\Keyboards\InlineKeyboardMarkup;

/**
 * Represents a link to a page containing an embedded video player or a video
 * file. By default, this video file will be sent by the user with an optional
 * caption. Alternatively, you can use input_message_content to send a message
 * with the specified content instead of the video.
 *
 * | If an InlineQueryResultVideo message contains an embedded video
 * | (e.g., YouTube), you must replace its content using
 * | input_message_content.
 *
 * @package tgbot\TelegramApi\Types\Inline
 * @see https://core.telegram.org/bots/api#inlinequeryresultvideo
 */
class Video extends InlineQueryResult
{
    /**
     * Type of the result, must be video
     * @var string
     */
    public $type = 'video';

    /**
     * Unique identifier for this result, 1-64 bytes
     * @var string
     */
    public $id = '';

    /**
     * A valid URL for the embedded video player or video file
     * @var string
     */
    public $video_url = '';

    /**
     * Mime type of the content of video url, “text/html” or “video/mp4”
     * @var string
     */
    public $mime_type = '';

    /**
     * URL of the thumbnail (jpeg only) for the video
     * @var string
     */
    public $thumb_url = '';

    /**
     * Title for the result
     * @var string
     */
    public $title = '';

    /**
     * Optional. Caption of the video to be sent, 0-1024 characters after
     * entities parsing
     * @var string
     */
    public $caption = '';

    /**
     * Optional. Send Markdown or HTML, if you want Telegram apps to show bold,
     * italic, fixed-width text or inline URLs in the media caption.
     * @var string
     */
    public $parse_mode = '';

    /**
     * Optional. Video width
     * @var int
     */
    public $video_width = 0;

    /**
     * Optional. Video height
     * @var int
     */
    public $video_height = 0;

    /**
     * Optional. Video duration in seconds
     * @var int
     */
    public $video_duration = 0;

    /**
     * Optional. Short description of the result
     * @var string
     */
    public $description = '';

    /**
     * Optional. Inline keyboard attached to the message
     * @var InlineKeyboardMarkup
     */
    public $reply_markup;

    /**
     * Optional. Content of the message to be sent instead of the video.
     * This field is required if InlineQueryResultVideo is used to send an
     * HTML-page as a result (e.g., a YouTube video).
     * @var InputMessageContent
     */
    public $input_message_content;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            InputMessageContent::class  => 'input_message_content',
            InlineKeyboardMarkup::class => 'reply_markup'
        ];
    }
}