<?php

namespace tgbot\TelegramApi\Telegram\Types;

use tgbot\TelegramApi\Abstracts\TelegramTypesAbstract;

/**
 * This object represents one result of an inline query.
 * Telegram clients currently support results of the following 20 types:
 *
 *   - InlineQueryResultCachedAudio
 *   - InlineQueryResultCachedDocument
 *   - InlineQueryResultCachedGif
 *   - InlineQueryResultCachedMpeg4Gif
 *   - InlineQueryResultCachedPhoto
 *   - InlineQueryResultCachedSticker
 *   - InlineQueryResultCachedVideo
 *   - InlineQueryResultCachedVoice
 *   - InlineQueryResultArticle
 *   - InlineQueryResultAudio
 *   - InlineQueryResultContact
 *   - InlineQueryResultGame
 *   - InlineQueryResultDocument
 *   - InlineQueryResultGif
 *   - InlineQueryResultLocation
 *   - InlineQueryResultMpeg4Gif
 *   - InlineQueryResultPhoto
 *   - InlineQueryResultVenue
 *   - InlineQueryResultVideo
 *   - InlineQueryResultVoice
 *
 * @see https://core.telegram.org/bots/api#inlinequeryresult
 */
class InlineQueryResult extends TelegramTypesAbstract
{
    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}