<?php

namespace tgbot\TelegramApi\Telegram\Types;

/**
 * Represents the content of a contact message to be sent as the result of an
 * inline query.
 * @package tgbot\TelegramApi\Types
 * @see https://core.telegram.org/bots/api#inputcontactmessagecontent
 */
class InputContactMessageContent extends InputMessageContent
{
    /**
     * Contact's phone number
     * @var string
     */
    public $phone_number = '';

    /**
     * Contact's first name
     * @var string
     */
    public $first_name = '';

    /**
     * Optional. Contact's last name
     * @var string
     */
    public $last_name = '';

    /**
     * Optional. Additional data about the contact in the form of a vCard, 0-2048 bytes
     * @var string
     */
    public $vcard = '';

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}