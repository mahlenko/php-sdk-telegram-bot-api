<?php

namespace tgbot\TelegramApi\Telegram\Types;

/**
 * Represents the content of a location message to be sent as the result of an
 * inline query.
 * @package tgbot\TelegramApi\Types
 * @see https://core.telegram.org/bots/api#inputlocationmessagecontent
 */
class InputLocationMessageContent extends InputMessageContent
{
    /**
     * Latitude of the location in degrees
     * @var float
     */
    public $latitude = 0.0;

    /**
     * Longitude of the location in degrees
     * @var float
     */
    public $longitude = 0.0;

    /**
     * Optional. Period in seconds for which the location can be updated,
     * should be between 60 and 86400.
     * @var int
     */
    public $live_period = 0;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}