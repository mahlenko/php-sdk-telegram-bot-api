<?php

namespace tgbot\TelegramApi\Telegram\Types;

use tgbot\TelegramApi\Abstracts\TelegramTypesAbstract;

/**
 * This object represents the content of a media message to be sent. It should be one of
 *  - InputMediaAnimation
 *  - InputMediaDocument
 *  - InputMediaAudio
 *  - InputMediaPhoto
 *  - InputMediaVideo
 * @see https://core.telegram.org/bots/api#inputmedia
 */
class InputMedia extends TelegramTypesAbstract
{
    /**
     * Type of the result
     * @var string
     */
    public $type = '';

    /**
     * File to send. Pass a file_id to send a file that exists on the Telegram
     * servers (recommended), pass an HTTP URL for Telegram to get a file from
     * the Internet, or pass “attach://<file_attach_name>” to upload a new one
     * using multipart/form-data under <file_attach_name> name.
     * @see https://core.telegram.org/bots/api#sending-files
     * @var string
     */
    public $media = '';

    /**
     * Optional. Caption of the photo to be sent, 0-1024 characters after
     * entities parsing
     * @var string
     */
    public $caption = '';

    /**
     * Optional. Send Markdown or HTML, if you want Telegram apps to show bold,
     * italic, fixed-width text or inline URLs in the media caption.
     * @var string
     */
    public $parse_mode = '';

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}