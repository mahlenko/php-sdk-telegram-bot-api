<?php

namespace tgbot\TelegramApi\Telegram\Types;

use tgbot\TelegramApi\Abstracts\TelegramTypesAbstract;

/**
 * This object represents the content of a message to be sent as a result of an
 * inline query. Telegram clients currently support the following 4 types:
 *
 * | InputTextMessageContent
 * | InputLocationMessageContent
 * | InputVenueMessageContent
 * | InputContactMessageContent
 *
 * @package tgbot\TelegramApi\Types
 */
class InputMessageContent extends TelegramTypesAbstract
{
    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}