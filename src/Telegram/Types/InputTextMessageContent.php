<?php

namespace tgbot\TelegramApi\Telegram\Types;

/**
 * Represents the content of a text message to be sent as the result of an inline query.
 * @package tgbot\TelegramApi\Types
 * @see https://core.telegram.org/bots/api#inputtextmessagecontent
 */
class InputTextMessageContent extends InputMessageContent
{
    /**
     * Text of the message to be sent, 1-4096 characters
     * @var string
     */
    public $message_text = '';

    /**
     * Optional. Send Markdown or HTML, if you want Telegram apps to show bold,
     * italic, fixed-width text or inline URLs in your bot's message.
     * @var string
     */
    public $parse_mode = '';

    /**
     * Optional. Disables link previews for links in the sent message
     * @var bool
     */
    public $disable_web_page_preview = false;
}