<?php

namespace tgbot\TelegramApi\Telegram\Types;

/**
 * Represents the content of a venue message to be sent as the result of an
 * inline query.
 * @package tgbot\TelegramApi\Types
 * @see https://core.telegram.org/bots/api#inputvenuemessagecontent
 */
class InputVenueMessageContent extends InputMessageContent
{
    /**
     * Latitude of the venue in degrees
     * @var float
     */
    public $latitude = 0.0;

    /**
     * Longitude of the venue in degrees
     * @var float
     */
    public $longitude = 0.0;

    /**
     * Name of the venue
     * @var string
     */
    public $title = '';

    /**
     * Address of the venue
     * @var string
     */
    public $address = '';

    /**
     * Optional. Foursquare identifier of the venue, if known
     * @var string
     */
    public $foursquare_id = '';

    /**
     * Optional. Foursquare type of the venue, if known. (For example,
     * “arts_entertainment/default”, “arts_entertainment/aquarium” or “food/icecream”.)
     * @var string
     */
    public $foursquare_type = '';
}