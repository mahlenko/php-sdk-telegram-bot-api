<?php


namespace tgbot\TelegramApi\Telegram\Types\Media;

use tgbot\TelegramApi\Telegram\Types\InputFile;
use tgbot\TelegramApi\Telegram\Types\InputMedia;

/**
 * Represents an audio file to be treated as music to be sent.
 * @see https://core.telegram.org/bots/api#inputmediaaudio
 */
class Audio extends InputMedia
{
    /**
     * Type of the result, must be video
     * @var string
     */
    public $type = 'audio';

    /**
     * Optional. Thumbnail of the file sent; can be ignored if thumbnail
     * generation for the file is supported server-side. The thumbnail should
     * be in JPEG format and less than 200 kB in size. A thumbnail‘s width and
     * height should not exceed 320. Ignored if the file is not uploaded using
     * multipart/form-data. Thumbnails can’t be reused and can be only uploaded
     * as a new file, so you can pass “attach://<file_attach_name>”
     * if the thumbnail was uploaded using multipart/form-data under <file_attach_name>.
     * @var InputFile
     */
    public $thumb;

    /**
     * Optional. Video duration
     * @var int
     */
    public $duration = 0;

    /**
     * Optional. Performer of the audio
     * @var string
     */
    public $performer = '';

    /**
     * Optional. Title of the audio
     * @var string
     */
    public $title = '';

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            InputFile::class => 'thumb'
        ];
    }

}