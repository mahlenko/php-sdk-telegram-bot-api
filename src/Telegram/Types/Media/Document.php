<?php


namespace tgbot\TelegramApi\Telegram\Types\Media;

use tgbot\TelegramApi\Telegram\Types\InputFile;
use tgbot\TelegramApi\Telegram\Types\InputMedia;

/**
 * Represents a general file to be sent.
 * @see https://core.telegram.org/bots/api#inputmediadocument
 */
class Document extends InputMedia
{
    /**
     * Type of the result, must be document
     * @var string
     */
    public $type = 'document';

    /**
     * Optional. Document thumbnail as defined by sender
     * @var InputFile
     */
    public $thumb;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            InputFile::class => 'thumb'
        ];
    }
}