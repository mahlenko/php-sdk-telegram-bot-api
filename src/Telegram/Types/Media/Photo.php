<?php

namespace tgbot\TelegramApi\Telegram\Types\Media;

use tgbot\TelegramApi\Telegram\Types\InputMedia;

/**
 * Represents a photo to be sent.
 * @see https://core.telegram.org/bots/api#inputmediaphoto
 */
class Photo extends InputMedia
{
    /**
     * Type of the result, must be photo
     * @var string
     */
    public $type = 'photo';
}