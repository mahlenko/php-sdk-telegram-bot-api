<?php

namespace tgbot\TelegramApi\Telegram\Types\Passport;

use tgbot\TelegramApi\Abstracts\TelegramTypesAbstract;

/**
 * Contains information about Telegram Passport data shared with the bot by the user.
 * @see https://core.telegram.org/bots/api#passportdata
 */
class PassportData extends TelegramTypesAbstract
{
    /**
     * Array with information about documents and other Telegram Passport
     * elements that was shared with the bot
     * @var EncryptedPassportElement[]
     */
    public $data = [];

    /**
     * Encrypted credentials required to decrypt the data
     * @var EncryptedCredentials
     */
    public $credentials;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            EncryptedCredentials::class     => 'credentials',
            EncryptedPassportElement::class => 'data:array'
        ];
    }
}