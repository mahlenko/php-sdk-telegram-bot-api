<?php

namespace tgbot\TelegramApi\Telegram\Types\Passport;

use tgbot\TelegramApi\Abstracts\TelegramTypesAbstract;

class PassportElementError extends TelegramTypesAbstract
{
    /**
     * Error source, must be data
     * @var string
     */
    public $source = '';

    /**
     * The section of the user's Telegram Passport which has the error, one of
     * “personal_details”, “passport”, “driver_license”, “identity_card”,
     * “internal_passport”, “address”
     * @var string
     */
    public $type = '';

    /**
     * Error message
     * @var string
     */
    public $message = '';

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}