<?php

namespace tgbot\TelegramApi\Telegram\Types\Passport\PassportElementError;

use tgbot\TelegramApi\Telegram\Types\Passport\PassportElementError;

/**
 * Represents an issue with a document scan. The error is considered resolved
 * when the file with the document scan changes.
 * @see https://core.telegram.org/bots/api#passportelementerrorfile
 */
class File extends PassportElementError
{
    /**
     * The section of the user's Telegram Passport which has the issue,
     * one of “utility_bill”, “bank_statement”, “rental_agreement”,
     * “passport_registration”, “temporary_registration”
     * @var string
     */
    public $type = '';

    /**
     * Base64-encoded file hash
     * @var string
     */
    public $file_hash = '';
}