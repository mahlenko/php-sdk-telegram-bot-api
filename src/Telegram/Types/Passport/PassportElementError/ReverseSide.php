<?php

namespace tgbot\TelegramApi\Telegram\Types\Passport\PassportElementError;

use tgbot\TelegramApi\Telegram\Types\Passport\PassportElementError;

/**
 * Represents an issue with the reverse side of a document. The error is considered
 * resolved when the file with reverse side of the document changes.
 * @see https://core.telegram.org/bots/api#passportelementerrorreverseside
 */
class ReverseSide extends PassportElementError
{
    /**
     * The section of the user's Telegram Passport which has the issue, one of
     * “driver_license”, “identity_card”
     * @var string
     */
    public $type = '';

    /**
     * Base64-encoded hash of the file with the reverse side of the document
     * @var string
     */
    public $file_hash = '';
}