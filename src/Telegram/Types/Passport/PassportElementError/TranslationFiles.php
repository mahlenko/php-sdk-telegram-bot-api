<?php

namespace tgbot\TelegramApi\Telegram\Types\Passport\PassportElementError;

use tgbot\TelegramApi\Telegram\Types\Passport\PassportElementError;

/**
 * Represents an issue with the translated version of a document.
 * The error is considered resolved when a file with the document
 * translation change.
 * @see https://core.telegram.org/bots/api#passportelementerrortranslationfiles
 */
class TranslationFiles extends PassportElementError
{
    /**
     * Type of element of the user's Telegram Passport which has the issue,
     * one of “passport”, “driver_license”, “identity_card”, “internal_passport”,
     * “utility_bill”, “bank_statement”, “rental_agreement”, “passport_registration”,
     * “temporary_registration”
     * @var string
     */
    public $type = '';

    /**
     * List of base64-encoded file hashes
     * @var array
     */
    public $file_hashes = [];
}