<?php

namespace tgbot\TelegramApi\Telegram\Types\Payments;

use tgbot\TelegramApi\Abstracts\TelegramTypesAbstract;

/**
 * This object represents information about an order.
 * @see https://core.telegram.org/bots/api#orderinfo
 */
class OrderInfo extends TelegramTypesAbstract
{
    /**
     * Optional. User name
     * @var string
     */
    public $name = '';

    /**
     * Optional. User's phone number
     * @var string
     */
    public $phone_number = '';

    /**
     * Optional. User email
     * @var string
     */
    public $email = '';

    /**
     * Optional. User shipping address
     * @var ShippingAddress
     */
    public $shipping_address;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            ShippingAddress::class => 'shipping_address'
        ];
    }
}