<?php

namespace tgbot\TelegramApi\Telegram\Types\Payments;

use tgbot\TelegramApi\Abstracts\TelegramTypesAbstract;

/**
 * This object represents a shipping address.
 * @see https://core.telegram.org/bots/api#shippingaddress
 */
class ShippingAddress extends TelegramTypesAbstract
{
    /**
     * ISO 3166-1 alpha-2 country code
     * @var string
     */
    public $country_code = '';

    /**
     * State, if applicable
     * @var string
     */
    public $state = '';

    /**
     * City
     * @var string
     */
    public $city = '';

    /**
     * First line for the address
     * @var string
     */
    public $street_line1 = '';

    /**
     * Second line for the address
     * @var string
     */
    public $street_line2 = '';

    /**
     * Address post code
     * @var string
     */
    public $post_code = '';

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}