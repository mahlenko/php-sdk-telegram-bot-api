<?php

namespace tgbot\TelegramApi\Telegram\Types\Payments;
use tgbot\TelegramApi\Abstracts\TelegramTypesAbstract;

/**
 * This object represents one shipping option.
 * @see https://core.telegram.org/bots/api#shippingoption
 */
class ShippingOption extends TelegramTypesAbstract
{
    /**
     * Shipping option identifier
     * @var string
     */
    public $id = '';

    /**
     * Option title
     * @var string
     */
    public $title = '';

    /**
     * List of price portions
     * @var LabeledPrice[]
     */
    public $prices = [];

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            LabeledPrice::class => 'prices:array'
        ];
    }
}