<?php

namespace tgbot\TelegramApi\Telegram\Types\Payments;

use tgbot\TelegramApi\Abstracts\TelegramTypesAbstract;
use tgbot\TelegramApi\Telegram\Types\User;

/**
 * This object contains information about an incoming shipping query.
 * @see https://core.telegram.org/bots/api#shippingquery
 */
class ShippingQuery extends TelegramTypesAbstract
{
    /**
     * Unique query identifier
     * @var string
     */
    public $id = '';

    /**
     * User who sent the query
     * @var User
     */
    public $from;

    /**
     * Bot specified invoice payload
     * @var string
     */
    public $invoice_payload = '';

    /**
     * User specified shipping address
     * @var ShippingAddress
     */
    public $shipping_address;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            User::class            => 'from',
            ShippingAddress::class => 'shipping_address'
        ];
    }
}