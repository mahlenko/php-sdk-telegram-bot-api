<?php

namespace tgbot\TelegramApi\Telegram\Types\Payments;

use tgbot\TelegramApi\Abstracts\TelegramTypesAbstract;

/**
 * This object contains basic information about a successful payment.
 * @see https://core.telegram.org/bots/api#successfulpayment
 */
class SuccessfulPayment extends TelegramTypesAbstract
{
    /**
     * Three-letter ISO 4217 currency code
     * @var string
     */
    public $currency = '';

    /**
     * Total price in the smallest units of the currency (integer, not float/double).
     * For example, for a price of US$ 1.45 pass amount = 145. See the exp parameter
     * in currencies.json, it shows the number of digits past the decimal point
     * for each currency (2 for the majority of currencies).
     * @see https://core.telegram.org/bots/payments/currencies.json
     * @var int
     */
    public $total_amount = 0;

    /**
     * Bot specified invoice payload
     * @var string
     */
    public $invoice_payload = '';

    /**
     * Optional. Identifier of the shipping option chosen by the user
     * @var string
     */
    public $shipping_option_id = '';

    /**
     * Optional. Order info provided by the user
     * @var OrderInfo
     */
    public $order_info;

    /**
     * Telegram payment identifier
     * @var string
     */
    public $telegram_payment_charge_id = '';

    /**
     * Provider payment identifier
     * @var string
     */
    public $provider_payment_charge_id = '';

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            OrderInfo::class => 'order_info'
        ];
    }
}