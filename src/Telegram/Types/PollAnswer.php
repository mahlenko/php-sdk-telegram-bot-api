<?php

namespace tgbot\TelegramApi\Telegram\Types;

use tgbot\TelegramApi\Abstracts\TelegramTypesAbstract;

/**
 * This object represents an answer of a user in a non-anonymous poll.
 * @see https://core.telegram.org/bots/api#pollanswer
 */
class PollAnswer extends TelegramTypesAbstract
{
    /**
     * Unique poll identifier
     * @var string
     */
    public $poll_id = '';

    /**
     * The user, who changed the answer to the poll
     * @var User
     */
    public $user;

    /**
     * 0-based identifiers of answer options, chosen by the user.
     * May be empty if the user retracted their vote.
     * @var array
     */
    public $option_ids = [];

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            User::class => 'user'
        ];
    }
}