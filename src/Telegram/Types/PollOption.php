<?php

namespace tgbot\TelegramApi\Telegram\Types;

use tgbot\TelegramApi\Abstracts\TelegramTypesAbstract;

/**
 * This object contains information about one answer option in a poll.
 * @see https://core.telegram.org/bots/api#polloption
 */
class PollOption extends TelegramTypesAbstract
{
    /**
     * Option text, 1-100 characters
     * @var string
     */
    public $text = '';

    /**
     * Number of users that voted for this option
     * @var int
     */
    public $voter_count = 0;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}