<?php

namespace tgbot\TelegramApi\Telegram\Types\Stickers;

use tgbot\TelegramApi\Abstracts\TelegramTypesAbstract;
use tgbot\TelegramApi\Telegram\Types\InputFile;
use tgbot\TelegramApi\Telegram\Types\PhotoSize;


/**
 * This object represents a sticker.
 * @see https://core.telegram.org/bots/api#stickers
 */
class Sticker extends TelegramTypesAbstract
{
    /**
     * Identifier for this file, which can be used to download or reuse the file
     * @var string
     */
    public $file_id = '';

    /**
     * Unique identifier for this file, which is supposed to be the same over time
     * and for different bots. Can't be used to download or reuse the file.
     * @var string
     */
    public $file_unique_id = '';

    /**
     * Sticker width
     * @var int
     */
    public $width = 0;

    /**
     * Sticker height
     * @var int
     */
    public $height = 0;

    /**
     * True, if the sticker is animated
     * @var bool
     */
    public $is_animated = false;

    /**
     * Optional. Sticker thumbnail in the .webp or .jpg format
     * @var InputFile
     */
    public $thumb;

    /**
     * Optional. Emoji associated with the sticker
     * @var string
     */
    public $emoji = '';

    /**
     * Optional. Name of the sticker set to which the sticker belongs
     * @var string
     */
    public $set_name = '';

    /**
     * Optional. For mask stickers, the position where the mask should be placed
     * @var MaskPosition
     */
    public $mask_position;

    /**
     * Optional. File size
     * @var int
     */
    public $file_size = 0;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            PhotoSize::class    => 'thumb',
            MaskPosition::class => 'mask_position'
        ];
    }
}