<?php

namespace tgbot\TelegramApi\Telegram\Types\Stickers;

use tgbot\TelegramApi\Abstracts\TelegramTypesAbstract;
use tgbot\TelegramApi\Telegram\Types\PhotoSize;


/**
 * This object represents a sticker set.
 * @see https://core.telegram.org/bots/api#stickerset
 */
class StickerSet extends TelegramTypesAbstract
{
    /**
     * Sticker set name
     * @var string
     */
    public $name = '';

    /**
     * Sticker set title
     * @var string
     */
    public $title = '';

    /**
     * True, if the sticker set contains animated stickers
     * @var bool
     */
    public $is_animated = false;

    /**
     * True, if the sticker set contains masks
     * @var bool
     */
    public $contains_masks = false;

    /**
     * List of all set stickers
     * @var Sticker[]
     */
    public $stickers = [];

    /**
     * Optional. Sticker set thumbnail in the .WEBP or .TGS format
     * @var PhotoSize
     */
    public $thumb;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            Sticker::class   => 'stickers:array',
            PhotoSize::class => 'thumb'
        ];
    }
}