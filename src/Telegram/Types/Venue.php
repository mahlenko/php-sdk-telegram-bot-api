<?php

namespace tgbot\TelegramApi\Telegram\Types;

use tgbot\TelegramApi\Abstracts\TelegramTypesAbstract;

/**
 * This object represents a venue.
 * @see https://core.telegram.org/bots/api#venue
 */
class Venue extends TelegramTypesAbstract
{
    /**
     * Venue location
     * @var Location
     */
    public $location;

    /**
     * Name of the venue
     * @var string
     */
    public $title = '';

    /**
     * Address of the venue
     * @var string
     */
    public $address = '';

    /**
     * Optional. Foursquare identifier of the venue
     * @var string
     */
    public $foursquare_id = '';

    /**
     * Optional. Foursquare type of the venue.
     * (For example, “arts_entertainment/default”,
     * “arts_entertainment/aquarium” or “food/icecream”.)
     * @var string
     */
    public $foursquare_type = '';

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            Location::class => 'location'
        ];
    }
}