<?php


namespace tgbot\TelegramApi\Telegram\Types;


use tgbot\TelegramApi\Abstracts\TelegramTypesAbstract;

class WebhookInfo extends TelegramTypesAbstract
{
    /**
     * Webhook URL, may be empty if webhook is not set up
     * @var string
     */
    public $url = '';

    /**
     * True, if a custom certificate was provided for webhook certificate checks
     * @var bool
     */
    public $has_custom_certificate = false;

    /**
     * Number of updates awaiting delivery
     * @var int
     */
    public $pending_update_count = 0;

    /**
     * Optional. Unix time for the most recent error that happened when trying
     * to deliver an update via webhook
     * @var int
     */
    public $last_error_date = 0;

    /**
     * Optional. Error message in human-readable format for the most recent error
     * that happened when trying to deliver an update via webhook
     * @var string
     */
    public $last_error_message = '';

    /**
     * Optional. Maximum allowed number of simultaneous HTTPS connections
     * to the webhook for update delivery
     * @var int
     */
    public $max_connections = 0;

    /**
     * Optional. A list of update types the bot is subscribed to.
     * Defaults to all update types
     * @var array
     */
    public $allowed_updates = [];

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [];
    }
}